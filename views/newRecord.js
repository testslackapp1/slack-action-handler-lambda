module.exports = {
  type: 'modal',
  title: {
    type: 'plain_text',
    text: 'Create new record'
  },
  submit: {
    type: 'plain_text',
    text: 'Create'
  },
  blocks: [
    {
      "type": "input",
      "block_id": "firstName",
      "label": {
        "type": "plain_text",
        "text": "First name"
      },
      "element": {
        "action_id": "content",
        "type": "plain_text_input",
        "placeholder": {
          "type": "plain_text",
          "text": "John"
        },
        "multiline": false
      }
    },
    {
      "type": "input",
      "block_id": "lastName",
      "label": {
        "type": "plain_text",
        "text": "Last name"
      },
      "element": {
        "action_id": "content",
        "type": "plain_text_input",
        "placeholder": {
          "type": "plain_text",
          "text": "Doe"
        },
        "multiline": false
      }
    }
  ]
};
