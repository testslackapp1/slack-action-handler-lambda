const AWS = require('aws-sdk');

const axios = require('axios');
const queryString = require('query-string');
const { v4: uuidv4 } = require('uuid');
const base64url = require('base64url');

const dynamoDb = new AWS.DynamoDB.DocumentClient({
  api_version: '2012-08-10',
  region: 'us-west-1'
});

const newRecordModal = require('./views/newRecord');

const apiUrl = 'https://slack.com/api';

exports.handler = async (request) => {
  const query = base64url.decode(request.body);
  const body = queryString.parse(query);

  const payloadStringified = body.payload;
  const payload = JSON.parse(payloadStringified);

  const { user, actions, trigger_id, type } = payload;

  if(type === 'block_actions' && actions && actions[0].action_id === 'add_record') {
    const data = {
      trigger_id,
      view: newRecordModal,
    };

    const params = {
      TableName: 'slackUsers',
      Key: {
        'id' : user.id,
      }
    };

    const record = await dynamoDb.get(params).promise();

    if (!record) {
      return { statusCode: 400, };
    }

    await axios.post(`${apiUrl}/views.open`, data, { headers: { Authorization: `Bearer ${record.Item.authToken}` } });
  }

  if (type === 'view_submission') {
    const {
      view: {
        state: {
          values: {
            firstName: { content: { value: firstNameValue } },
            lastName: { content: { value: lastNameValue } },
          }
        }
      }
    } = payload;

    const params = {
      TableName: 'testSlackAppRecords',
      Item: {
        'id' : uuidv4(),
        'firstName' : firstNameValue,
        'lastName' : lastNameValue,
        'slackUserId' : user.id,
      }
    };

    try {
      await dynamoDb.put(params).promise();
      return { statusCode: 200 };
    } catch (e) {
      return { statusCode: 400 };
    }
  }

  return { statusCode: 200 };
};
